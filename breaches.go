package hibp

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type BreachesService struct {
	client *Client
}

type Breach struct {
	Name         string    `json:"Name"`
	Title        string    `json:"Title"`
	Domain       string    `json:"Domain"`
	BreachDate   string    `json:"BreachDate"`
	AddedDate    time.Time `json:"AddedDate"`
	ModifiedDate time.Time `json:"ModifiedDate"`
	PwnCount     int       `json:"PwnCount"`
	Description  string    `json:"Description"`
	LogoPath     string    `json:"LogoPath"`
	DataClasses  []string  `json:"DataClasses"`
	IsVerified   bool      `json:"IsVerified"`
	IsFabricated bool      `json:"IsFabricated"`
	IsSensitive  bool      `json:"IsSensitive"`
	IsRetired    bool      `json:"IsRetired"`
	IsSpamList   bool      `json:"IsSpamList"`
}

func (s *BreachesService) ListBreaches() ([]*Breach, *http.Response, error) {
	apiMethod := "breaches"
	req, err := s.client.NewRequest(http.MethodGet, apiMethod, nil)

	if err != nil {
		panic(err)
	}
	resp, err := s.client.Do(req)

	if err != nil {
		panic(err)
	}
	body, err := io.ReadAll(resp.Body)

	if err != nil {
		resp.Body.Close()
		return nil, nil, err
	}
	defer resp.Body.Close()

	var respObj []*Breach
	err = json.Unmarshal(body, &respObj)
	if err != nil {
		return nil, nil, err
	}
	return respObj, resp, err
}

func (s *BreachesService) GetBreach(name string) (*Breach, *http.Response, error) {
	apiMethod := fmt.Sprintf("breach/%s", name)
	req, err := s.client.NewRequest(http.MethodGet, apiMethod, nil)

	if err != nil {
		return nil, nil, err
	}
	resp, err := s.client.Do(req)

	if err != nil {
		return nil, nil, err
	}
	body, err := io.ReadAll(resp.Body)

	if err != nil {
		resp.Body.Close()
		return nil, nil, err
	}
	defer resp.Body.Close()

	var respObj *Breach
	err = json.Unmarshal(body, &respObj)
	if err != nil {
		return nil, nil, err
	}
	return respObj, resp, err
}

type ByAccountOpts struct {
	Domain            string
	TruncateResponse  bool
	IncludeUnverified bool
}

func (s *BreachesService) ByAccount(accountName string) ([]*Breach, *http.Response, error) {
	apiMethod := fmt.Sprintf("breachedaccount/%s", accountName)
	req, err := s.client.NewRequest(http.MethodGet, apiMethod, nil)

	q := req.URL.Query()
	q.Add("truncateResponse", "false")

	req.URL.RawQuery = q.Encode()

	if err != nil {
		return nil, nil, err
	}
	resp, err := s.client.Do(req)

	if err != nil {
		return nil, nil, err
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		resp.Body.Close()
		return nil, nil, err
	}
	defer resp.Body.Close()

	var respObj []*Breach
	if resp.StatusCode == 200 {
		err = json.Unmarshal(body, &respObj)
		if err != nil {
			return nil, nil, err
		}
	}

	if len(respObj) == 0 {
		return nil, resp, nil
	}

	return respObj, resp, nil
}
