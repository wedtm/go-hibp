package hibp

import (
	"log"
	"os"
	"testing"
)

func TestGetBreaches(t *testing.T) {
	client, err := NewClient(os.Getenv("HIBP_API_KEY"), nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.ListBreaches()

	if err != nil {
		t.Fail()
	}

	t.Logf("Breaches: %d", len(bs))
}

func TestGetAnonBreaches(t *testing.T) {
	client, err := NewClient("", nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.ListBreaches()

	if err != nil {
		t.Fail()
	}

	t.Logf("Anon Breaches: %d", len(bs))
}

func TestGetBreach(t *testing.T) {
	client, err := NewClient(os.Getenv("HIBP_API_KEY"), nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.GetBreach("Adobe")

	if err != nil {
		t.Fail()
	}

	t.Logf("Breach: %v", bs)
}

func NoBreach(t *testing.T) {
	client, err := NewClient(os.Getenv("HIBP_API_KEY"), nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.GetBreach("FakeBreach")

	if err != nil {
		t.Fail()
	}

	if bs != nil {
		t.Fail()
	}
}

func TestBreachedAccount(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.ByAccount("account-exists@hibp-integration-tests.com")

	if err != nil {
		t.Fail()
	}

	t.Logf("Account Breaches: %v", bs)
}

func TestBreachedAccountNotFound(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.ByAccount("not-active-breach@hibp-integration-tests.com")

	if err != nil {
		t.Fail()
	}

	t.Logf("Account Breaches: %v", bs)
}

func TestSpecial(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Breaches.ByAccount("notfound@zzz.com")

	if err != nil {
		t.Fail()
	}

	t.Logf("Account Breaches: %v", bs)
}
