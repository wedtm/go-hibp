package hibp

import (
	"os"
	"testing"
)

func TestPasswordsBySHA1Prefix(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		t.FailNow()
	}

	pass, _, err := client.Passwords.GetPasswordsBySHA1Prefix("5BAA6") // Password: password
	if err != nil {
		t.Logf("%v", err)
	}
	t.Logf("Passwords: %v", pass)
}

func TestPasswordsBySHA1(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		t.FailNow()
	}

	pass, _, err := client.Passwords.GetExactPasswordBySHA1("5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8") // Password: password
	if err != nil {
		t.Logf("%v", err)
		t.Fail()
	}

	if pass == nil {
		t.Logf("password: %v", pass)
		t.Fail()
	}
}
