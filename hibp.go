package hibp

import (
	"context"
	"net/http"
	"net/url"
	"strings"
)

const (
	defaultBaseURL = "https://haveibeenpwned.com/"
	apiVersionPath = "api/v3/"
	userAgent      = "go-hibp"
)

type Client struct {
	client    *http.Client
	baseURL   *url.URL
	apiKey    string
	UserAgent string

	Breaches  *BreachesService
	Pastes    *PastesService
	Passwords *PasswordsService
}

type ClientOptionFunc func(*Client) error

func NewClient(apiKey string, options ...ClientOptionFunc) (*Client, error) {
	client, err := newClient(options...)
	if err != nil {
		return nil, err
	}
	client.apiKey = apiKey
	client.baseURL, err = url.Parse(defaultBaseURL + apiVersionPath)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func newClient(options ...ClientOptionFunc) (*Client, error) {
	c := &Client{UserAgent: userAgent}

	// Configure the HTTP client.
	c.client = &http.Client{}

	// Apply any given client options.
	for _, fn := range options {
		if fn == nil {
			continue
		}
		if err := fn(c); err != nil {
			return nil, err
		}
	}

	// Create all the public services.
	c.Breaches = &BreachesService{client: c}
	c.Pastes = &PastesService{client: c}
	c.Passwords = &PasswordsService{client: c}

	return c, nil
}

func (c *Client) NewRequest(method, path string, opt interface{}) (*http.Request, error) {
	unescaped, err := url.PathUnescape(path)
	if err != nil {
		return nil, err
	}
	u := *c.baseURL
	// Set the encoded path data
	if strings.HasPrefix(unescaped, "https://") {
		newUrl, err := url.Parse(path)
		if err != nil {
			panic(err)
		}
		u = *newUrl
	} else {
		u.RawPath = c.baseURL.Path + path
		u.Path = c.baseURL.Path + unescaped
	}

	// Create a request specific headers map.
	reqHeaders := make(http.Header)
	reqHeaders.Set("Accept", "application/json")

	if c.UserAgent != "" {
		reqHeaders.Set("User-Agent", c.UserAgent)
	}

	req, err := http.NewRequest(method, u.String(), nil)
	if err != nil {
		return nil, err
	}

	for k, v := range reqHeaders {
		req.Header[k] = v
	}

	return req, nil
}

func (c *Client) Do(req *http.Request) (*http.Response, error) {
	req.Header["hibp-api-key"] = []string{c.apiKey}
	resp, err := c.client.Do(req)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

type RateLimiter interface {
	Wait(context.Context) error
}
