package hibp

import (
	"bufio"
	"crypto/sha1"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

type PasswordsService struct {
	client *Client
}

type PasswordMatch struct {
	Hash  string
	Count int64
}

func (s *PasswordsService) GetPasswordsBySHA1Prefix(hash string) ([]*PasswordMatch, *http.Response, error) {

	prefix := hash[:5]
	apiMethod := fmt.Sprintf("https://api.pwnedpasswords.com/range/%s", prefix)
	req, err := s.client.NewRequest(http.MethodGet, apiMethod, nil)

	if err != nil {
		return nil, nil, err
	}
	resp, err := s.client.Do(req)

	if err != nil {
		return nil, nil, err
	}

	if resp.StatusCode == 404 {
		return nil, nil, nil
	}

	scanner := bufio.NewScanner(resp.Body)
	defer resp.Body.Close()

	var respObj []*PasswordMatch

	for scanner.Scan() {
		hashString := scanner.Text()
		parts := strings.Split(hashString, ":")
		count, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			panic(err)
		}

		completeHash := prefix + parts[0]

		if completeHash[0:len(hash)] == hash {
			respObj = append(respObj, &PasswordMatch{
				Hash:  parts[0],
				Count: count,
			})
		}
	}

	return respObj, resp, err
}

func (s *PasswordsService) GetExactPasswordBySHA1(hash string) (*PasswordMatch, *http.Response, error) {
	pws, resp, err := s.client.Passwords.GetPasswordsBySHA1Prefix(hash[:5])
	if err != nil {
		panic(err)
	}
	for _, pw := range pws {
		if pw.Hash == hash[5:] {
			return pw, resp, nil
		}
	}
	return nil, resp, nil
}

func (s *PasswordsService) GetExactPasswordByPlaintext(password string) (*PasswordMatch, *http.Response, error) {
	hasher := sha1.New()
	bytes := hasher.Sum([]byte(password))
	return s.GetExactPasswordBySHA1(string(bytes))
}
