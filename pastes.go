package hibp

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type PastesService struct {
	client *Client
}

type Paste struct {
	Source     string    `json:"Source"`
	Id         string    `json:"Id"`
	Title      string    `json:"Title"`
	Date       time.Time `json:"Date"`
	EmailCount int64     `json:"EmailCount"`
}

func (s *PastesService) GetPastesByAccount(account string) ([]*Paste, *http.Response, error) {
	apiMethod := fmt.Sprintf("pasteaccount/%s", account)
	req, err := s.client.NewRequest(http.MethodGet, apiMethod, nil)

	if err != nil {
		panic(err)
	}
	resp, err := s.client.Do(req)

	if err != nil {
		panic(err)
	}

	if resp.StatusCode == 404 {
		return nil, nil, nil
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		resp.Body.Close()
		return nil, nil, err
	}
	defer resp.Body.Close()

	var respObj []*Paste
	err = json.Unmarshal(body, &respObj)
	if err != nil {
		return nil, nil, err
	}
	return respObj, resp, err
}
