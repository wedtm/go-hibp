package hibp

import (
	"log"
	"os"
	"testing"
)

func TestGetPastes(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Pastes.GetPastesByAccount("test@test.com")

	if err != nil {
		t.Failed()
	}
	t.Logf("Pastes: %v", bs)
}

func TestNoAccountPastes(t *testing.T) {
	apiKey := os.Getenv("HIBP_API_KEY")
	client, err := NewClient(apiKey, nil)
	if err != nil {
		test := err.Error()
		log.Println(test)
		panic(err)
	}
	bs, _, err := client.Pastes.GetPastesByAccount("test@thisbetterneverevereverrrexist.com")

	if err != nil {
		t.Failed()
	}

	t.Logf("No Account Pastes: %v", bs)
}
